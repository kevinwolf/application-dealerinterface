import { createActions } from 'alt/utils/decorators';
import flux from 'flux';
import RouterService from 'services/RouterService';

@createActions(flux)
class AuthActions {

  constructor () {
    this.generateActions('authenticate', 'setUser');
  }

  loginSuccessful (response) {
    RouterService.get().transitionTo(RouterService.getNextRoute());
    AuthActions.setUser(response.data.customer);
  }

  logout () {
    RouterService.get().transitionTo('login');
    this.dispatch();
  }

}

export default AuthActions;
