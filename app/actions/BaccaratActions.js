import { createActions } from 'alt/utils/decorators';
import flux from 'flux';
import io from 'socket.io-client';
import AuthStore from 'stores/AuthStore';
import BaccaratStore from 'stores/BaccaratStore';

let socket;

@createActions(flux)
class BaccaratActions {

  constructor () {
    this.generateActions('getGameData');
  }

  connect () {
    socket = io.connect('10.28.10.85:1101/bcserver');

    socket.on('gameData', (data) => {
      BaccaratActions.getGameData(data);
    });
  }

  disconnect () {
    socket.disconnect();
  }

  addPlayer (position) {
    socket.emit('onAddPlayer', position, AuthStore.state.user);
    this.dispatch(position);
  }

  removePlayer (position) {
    socket.emit('onRemovePlayer', position);
    this.dispatch();
  }

  addCard (card) {
    socket.emit('onScanCard', card);
  }

  sendDecision (desicion) {
    socket.emit('onDesicion', BaccaratStore.state.myPosition, desicion);
  }

}

export default BaccaratActions;
