import { createActions } from 'alt/utils/decorators';
import flux from 'flux';
import io from 'socket.io-client';
import AuthStore from 'stores/AuthStore';
import BlackjackStore from 'stores/BlackjackStore';

let socket;

@createActions(flux)
class BlackjackActions {

  constructor () {
    this.generateActions('getGameData');
  }

  connect () {
    socket = io.connect('10.28.10.85:1101/bjserver');

    socket.on('gameData', (data) => {
      BlackjackActions.getGameData(data);
    });
  }

  disconnect () {
    socket.disconnect();
  }

  addPlayer (position) {
    socket.emit('onAddPlayer', position, AuthStore.state.user);
    this.dispatch(position);
  }

  removePlayer (position) {
    socket.emit('onRemovePlayer', position);
    this.dispatch();
  }

  addCard (card) {
    socket.emit('onScanCard', card);
  }

  sendDecision (desicion) {
    socket.emit('onDesicion', BlackjackStore.state.myPosition, desicion);
  }

}

export default BlackjackActions;
