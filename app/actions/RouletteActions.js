import { createActions } from 'alt/utils/decorators';
import flux from 'flux';
import io from 'socket.io-client';
import AuthStore from 'stores/AuthStore';
import RouletteStore from 'stores/RouletteStore';

let socket;

@createActions(flux)
class RouletteActions {

  constructor () {
    this.generateActions('getGameData');
  }

  connect () {
    socket = io.connect('10.28.10.85:1101/rlserver');
    socket.on('gameData', (data) => {
      RouletteActions.getGameData(data);
    });
  }

  disconnect () {
    socket.disconnect();
  }

  addPlayer (position) {
    socket.emit('onAddPlayer', position, AuthStore.state.user);
    this.dispatch(position);
  }

  removePlayer (position) {
    socket.emit('onRemovePlayer', position);
    this.dispatch();
  }

  addCard (card) {
    socket.emit('onScanCard', card);
  }

  sendDecision (desicion) {
    socket.emit('onDesicion', RouletteStore.state.myPosition, desicion);
  }

}

export default RouletteActions;
