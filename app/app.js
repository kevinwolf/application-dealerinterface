// Third parties.
import React from 'react';
import Router from 'react-router';
import qs from 'query-string';

// Services - Actions - Stores.
import RouterService from 'services/RouterService';
import AuthActions from 'actions/AuthActions';

// Routes definition.
import Routes from 'routes';

// Create the router.
const router = Router.create({
  routes         : Routes,
  scrollBehavior : Router.ScrollToTopBehavior,
});

// Set the RouterService.
RouterService.set(router);

// If the token exists on localStorage, then
// login the user.
const token = localStorage.getItem('token') ?
  localStorage.getItem('token') :
  qs.parse(location.search).token;

if (token) {
  AuthActions.authenticate(token);
}

// Render application.
router.run((Handler, state) => React.render(<Handler {...state} />, document.body));
