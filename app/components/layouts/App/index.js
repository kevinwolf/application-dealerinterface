import React from 'react';
import { RouteHandler } from 'react-router';

// import RouterService from 'services/RouterService';
// import AuthStore from 'stores/AuthStore';

require('./style.scss');

class App extends React.Component {

  static contextTypes = {
    router : React.PropTypes.func,
  }

  render () {
    return <RouteHandler />;
  }

  // static willTransitionTo (transition) {
  //   // console.log(transition);
  // /*  if (!AuthStore.state.user) {
  //     RouterService.setNextRoute(transition.path);
  //     transition.redirect('login');
  //   }*/
  // }

}

export default App;
