import React, { Component } from 'react';
import connectToStores from 'alt/utils/connectToStores';

import AuthStore from 'stores/AuthStore';
import BaccaratStore from 'stores/BaccaratStore';
import BaccaratActions from 'actions/BaccaratActions';

import Chat from 'apogeelive.ui.chat';
import CardScanner from 'apogeelive.ui.cardScanner';

import { Grid, Row, Column } from 'components/ui/Layout';
import DealerBacSlot from 'components/ui/DealerBacSlot';
import DecisionButton from 'components/ui/DecisionButton';

const styles = {
  'DealerInfo' : {
    backgroundColor : '#393937',
    border          : '1px solid white',
    flexDirection   : 'column',
    flexGrow        : 3,
    fontWeight      : 'bold',
    fontSize        : '1.4em',
  },
  'ChatWindow' : {
    backgroundColor : '#808080',
    flexDirection   : 'column',
    flexGrow        : 1.5,
    color           : '#98B830',
    fontWeight      : 'bold',
    fontSize        : '1.9em',
  },
  'GameID' : {
    backgroundColor : '#000',
    fontWeight      : 'bold',
    color           : '#9D9D9D',
    fontSize        : '2em',
  },
  'FirstCol' : {
    backgroundColor : '#000',
    fontWeight      : 'bold',
    color           : '#9D9D9D',
    fontSize        : '1.5em',
    flexDirection   : 'column',
    height          : '40vh',
    textAlign       : 'center',
  },
  'Tie' : {
    backgroundColor : '#3CAA37',
    color           : '#fff',
    fontSize        : '1.4em',
  },
  'Player' : {
    backgroundColor : 'blue',
    color           : '#fff',
    fontSize        : '1.4em',
  },
  'Banker' : {
    backgroundColor : 'red',
    color           : '#fff',
    fontSize        : '1.4em',
  },
  'SecondCol' : {
    backgroundColor : '#000',
    fontWeight      : 'bold',
    color           : '#fff',
    fontSize        : '1.8em',
    flexDirection   : 'column',
    height          : '30vh',
  },
  'ThirdCol' : {
    backgroundColor : '#000',
    fontWeight      : 'bold',
    color           : '#fff',
    fontSize        : '3em',
    flexDirection   : 'column',
    height          : '5vh',
    textAlign       : 'center',
  },
  'TitleCol' : {
    backgroundColor : '#DADADA',
    fontWeight      : 'bold',
    color           : '#393937',
    fontSize        : '1.50em',
    flexDirection   : 'column',
    height          : '4vh',
  },
};

@connectToStores
class Baccarat extends Component {

  static propTypes = {
    'dealer'       : React.PropTypes.object,
    'game'         : React.PropTypes.object,
    'myPosition'   : React.PropTypes.number,
    'players'      : React.PropTypes.object,
    'winners'      : React.PropTypes.array,
    'winnersNames' : React.PropTypes.array,
  }

  componentWillMount () {
    BaccaratActions.connect();
  }

  componentWillUnmount () {
    BaccaratActions.disconnect();
  }

  static getStores () {
    return [ BaccaratStore ];
  }

  static getPropsFromStores () {
    return BaccaratStore.getState();
  }

  render () {
    let buttonsC;
    let buttons;
    let chat;
    let countPlayers;
    let winnersType;
    let winnersList;

    const isDealer = window.location.hash.indexOf('dealer') !== -1;

    if (this.props.players[1] !== void 0) {
      countPlayers = Object.keys(this.props.players[1].connected).length - 1;
    } else {
      countPlayers = 0;
    }

    if (this.props.winners) {
      winnersType = this.props.winners.map(winner => {
        let objToReturn;
        if (winner === 'TIE') {
          objToReturn = <span style={styles.Tie}>{winner}</span>;
        } else if (winner === 'PLAYER') {
          objToReturn = <span style={styles.Player}>{winner}</span>;
        } else {
          objToReturn = <span style={styles.Banker}>{winner}</span>;
        }
        return objToReturn;
      }
    );
    }
    if (this.props.winnersNames) {
      winnersList = this.props.winnersNames.map(name => <span>{name}</span>);
    }
    if (isDealer) {
      buttons = [];
      buttons.push(<Row>
        <Column text-center><DecisionButton action="MISTAKE" /></Column>
        <Column text-center><DecisionButton style={styles.DealerButtons} action="CHAT" /></Column>
        <Column text-center><DecisionButton style={styles.DealerButtons} action="CARDS" /></Column>
        <Column text-center><DecisionButton style={styles.DealerButtons} action="CHANGE ME" /></Column>
        </Row>);
      buttonsC = <CardScanner onScan={this._onScan} />;
    }

    if ((isDealer && this.props.dealer) || this.props.myPosition) {
      chat = (
        <Column style={styles.ChatWindow}>
          <Chat
            socket="http://10.28.10.85:1101/chat"
            room="BCTest"
            mode={isDealer ? 'dealer' : 'player'}
            user={isDealer ? this.props.dealer.nickname : AuthStore.state.user.NickName} />
        </Column>
      );
    } else {
      chat = <Column>Sit to see the chat</Column>;
    }

    return (
      <Grid>
      <Row >
          <Column text-center style={styles.GameID}>Game ID : 12368</Column>
          <Column text-center style={styles.GameID}>Dealer : Pamela</Column>
          <Column text-right-padding>{buttons}</Column>
      </Row>
        <Row >
          <Column>
            <Grid>
              <Row style={styles.FirstCol}>
              {winnersType}
              </Row>
              <Row style={styles.TitleCol}><Column text-center>Winners List</Column></Row>
              <Row style={styles.SecondCol}>
              {winnersList}
              </Row>
              <Row style={styles.TitleCol}><Column text-center>Total Players</Column></Row>
              <Row style={styles.ThirdCol}><Column>{countPlayers}</Column></Row>
            </Grid>
          </Column>
            <DealerBacSlot style={styles.DealerInfo} dataDealer={this.props.dealer} dataPlayers={this.props.players[1]}/>
            {chat}
            {buttonsC}
        </Row>
      </Grid>
    );
  }

  _onScan = card => {
    BaccaratActions.addCard(card);
  }

}

export default Baccarat;
