import React, { Component } from 'react';
import connectToStores from 'alt/utils/connectToStores';

import AuthStore from 'stores/AuthStore';
import BlackjackStore from 'stores/BlackjackStore';
import BlackjackActions from 'actions/BlackjackActions';

import Chat from 'apogeelive.ui.chat';
import CardScanner from 'apogeelive.ui.cardScanner';

import { Grid, Row, Column } from 'components/ui/Layout';
import PlayerSlot from 'components/ui/PlayerSlot';
import DecisionButton from 'components/ui/DecisionButton';

const styles = {
  'DealerMessage' : {
    backgroundColor : '#3caa37',
    border          : '1px solid white',
    flexDirection   : 'column',
    color           : '#fff',
    fontWeight      : 'bold',
    fontSize        : '1.4em',
  },
  'ChatWindow' : {
    backgroundColor : '#808080',
    flexDirection   : 'column',
    flexGrow        : 4.2,
    color           : '#359bca',
    fontWeight      : 'bold',
    fontSize        : '1.4em',
    height          : '40vh',
  },
  'GameID' : {
    backgroundColor : '#000',
    fontWeight      : 'bold',
    color           : '#9D9D9D',
    fontSize        : '1.4em',
  },
};

@connectToStores
class Blackjack extends Component {

  static propTypes = {
    'dealer'     : React.PropTypes.object,
    'game'       : React.PropTypes.object,
    'myPosition' : React.PropTypes.number,
    'players'    : React.PropTypes.object,
  }

  componentWillMount () {
    BlackjackActions.connect();
  }

  componentWillUnmount () {
    BlackjackActions.disconnect();
  }

  static getStores () {
    return [ BlackjackStore ];
  }

  static getPropsFromStores () {
    return BlackjackStore.getState();
  }

  render () {
    let buttonsC;
    let buttons;
    let chat;

    const isDealer = window.location.hash.indexOf('dealer') !== -1;
    const playerSlots = [ 1, 2, 3, 4, 5, 6, 7 ].map(i => {
      return <PlayerSlot key={i} data={this.props.players[i]} position={i} isDealer={isDealer} />;
    });

    if (isDealer) {
      buttons = [];
      buttons.push(<Row><Column text-center><DecisionButton action="MISTAKE" /></Column><Column text-center><DecisionButton style={styles.DealerButtons} action="CHAT" /></Column><Column text-center><DecisionButton style={styles.DealerButtons} action="CARDS" /></Column><Column text-center><DecisionButton style={styles.DealerButtons} action="CHANGE ME" /></Column></Row>);
      buttonsC = <CardScanner onScan={this._onScan} />;
    }

    if ((isDealer && this.props.dealer) || this.props.myPosition) {
      chat = (
        <Column style={styles.ChatWindow}>
          <Chat
            socket="http://10.28.10.85:1101/chat"
            room="BJTest"
            mode={isDealer ? 'dealer' : 'player'}
            user={isDealer ? this.props.dealer.nickname : AuthStore.state.user.NickName} />
        </Column>
      );
    } else {
      chat = <Column>Sit to see the chat</Column>;
    }

    return (
      <Grid>
      <Row >
          <Column text-center style={styles.GameID}>Game ID : 12368</Column>
          <Column text-center style={styles.DealerMessage}>PLAYER ON BOX 7</Column>
          <Column text-right-padding>{buttons}</Column>
      </Row>
        <Row style={{ height : '50vh' }}>
          {playerSlots}
        </Row>
        <Row style={{ height : '40vh' }}>
          {chat}
          <PlayerSlot data={this.props.dealer}  />
        </Row>
        {buttonsC}
      </Grid>
    );
  }

  _onScan = card => {
    BlackjackActions.addCard(card);
  }

}

export default Blackjack;
