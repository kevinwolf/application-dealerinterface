import React, { Component } from 'react';
import connectToStores from 'alt/utils/connectToStores';

import AuthStore from 'stores/AuthStore';
import RouletteStore from 'stores/RouletteStore';
import RouletteActions from 'actions/RouletteActions';

import Chat from 'apogeelive.ui.chat';
import CardScanner from 'apogeelive.ui.cardScanner';

import { Grid, Row, Column } from 'components/ui/Layout';
import DecisionButton from 'components/ui/DecisionButton';

import ProgressBar from 'progressbar.js';

// TODO put this shit in other file .
const styles = {
  'DealerInfo' : {
    backgroundColor : '#393937',
    border          : '1px solid white',
    flexDirection   : 'column',
    flexGrow        : 3,
    fontWeight      : 'bold',
    fontSize        : '1.4em',
  },
  'CenterColumn' : {
    flexDirection : 'column',
    flexGrow      : 3,
    color         : '#98B830',
    fontWeight    : 'bold',
    fontSize      : '1.5em',
  },
  'ChatWindow' : {
    backgroundColor : '#808080',
    flexDirection   : 'column',
    color           : '#98B830',
    fontWeight      : 'bold',
    border          : '6px solid white',
    height          : '61.5vh',
    width           : '130vh',
  },
  'GameID' : {
    backgroundColor : '#000',
    fontWeight      : 'bold',
    color           : '#9D9D9D',
    fontSize        : '2em',
  },
  'FirstRowRight' : {
    backgroundColor : '#000',
    fontWeight      : 'bold',
    color           : '#9D9D9D',
    fontSize        : '1.5em',
    flexDirection   : 'column',
    height          : '10vh',
    textAlign       : 'center',
  },
  'Tie' : {
    backgroundColor : '#3CAA37',
    color           : '#fff',
    fontSize        : '1.4em',
  },
  'Player' : {
    backgroundColor : '#000',
    color           : '#fff',
    fontSize        : '1.4em',
  },
  'Banker' : {
    backgroundColor : 'red',
    color           : '#fff',
    fontSize        : '1.4em',
  },
  'SecondCol' : {
    backgroundColor : '#000',
    fontWeight      : 'bold',
    color           : '#fff',
    fontSize        : '1.8em',
    flexDirection   : 'column',
    height          : '30vh',
    border          : '6px solid white',
  },
  'RightColumn' : {
    backgroundColor : '#000',
    fontWeight      : 'bold',
    color           : '#fff',
    fontSize        : '1.8em',
    flexDirection   : 'column',
    height          : '57.5vh',
    border          : '6px solid white',
    textAlign       : 'center',
  },
  'ThirdCol' : {
    backgroundColor : '#000',
    fontWeight      : 'bold',
    color           : '#fff',
    fontSize        : '3em',
    flexDirection   : 'column',
    height          : '11vh',
    textAlign       : 'center',
    border          : '6px solid white',

  },
  'TitleCol' : {
    backgroundColor : '#DADADA',
    fontWeight      : 'bold',
    color           : '#393937',
    fontSize        : '1.50em',
    flexDirection   : 'column',
    height          : '4vh',
  },
  'HeaderRowCenter' : {
    textAlign       : 'left',
    height          : '12vh',
    backgroundColor : '#000',
    borderLeft      : '6px solid #fff',
    borderRight     : '6px solid #fff',
    paddingTop      : '5vh',
    paddingLeft     : '5vh',
    'info'          : {
      color      : '#fff',
      fontWeight : 'bold',
      fontSize   : '1.85em',
      border     : '3px solid #fff',
      height     : '50%',
      flexGrow   : 8,
    },
    'stop' : {
      backgroundColor : 'red',
      borderRadius    : '100%',
      border          : '3px solid red',
      height          : '5vh',
      width           : '5vh',
    },
    'wait' : {
      backgroundColor : 'yellow',
      borderRadius    : '100%',
      border          : '3px solid yellow',
      height          : '5vh',
      width           : '5vh',
    },
    'go' : {
      backgroundColor : 'green',
      borderRadius    : '100%',
      border          : '3px solid green',
      height          : '5vh',
      width           : '5vh',
    },
    'disable' : {
      backgroundColor : 'grey',
      borderRadius    : '100%',
      border          : '3px solid grey',
      height          : '5vh',
      width           : '5vh',
    },
  },
  'HeaderSndRowCenter' : {
    textAlign       : 'left',
    height          : '3vh',
    backgroundColor : '#000',
    borderLeft      : '6px solid #fff',
    borderRight     : '6px solid #fff',
    paddingLeft     : '5vh',
  },
  'InfoRow' : {
    textAlign       : 'left',
    backgroundColor : '#000',
    height          : '20vh',
    borderLeft      : '6px solid #fff',
    borderRight     : '3px solid #fff',
    margin          : '0px',
    'Container'     : {
      paddingLeft : '5vh',
      paddingTop  : '3vh',
      margin      : '0px',
    },
  },
  'SpeedBox' : {
    backgroundColor : 'green',
    margin          : '0px',
  },
  'SpeedBoxRow' : {
    textAlign       : 'center',
    paddingLeft     : '25%',
    paddingRight    : '50%',
    backgroundColor : '#000',
    borderLeft      : '6px solid #fff',
    borderRight     : '6px solid #fff',
    margin          : '0px',
    'Container'     : {
      backgroundColor : '#fff',
      marginTop       : '2vh',
      height          : '5vh',
      width           : '20vh',
      margin          : '0px',
    },
  },
  'SpeedBoxMiddle' : {
    textAlign       : 'center',
    paddingLeft     : '25%',
    paddingRight    : '50%',
    backgroundColor : '#000',
    borderLeft      : '6px solid #fff',
    borderRight     : '6px solid #fff',
    margin          : '0px',
    'Container'     : {
      backgroundColor : '#fff',
      height          : '5vh',
      width           : '20vh',
      margin          : '0px',
    },
  },
  'SpeedBoxRowEnd' : {
    textAlign       : 'center',
    paddingLeft     : '25%',
    paddingRight    : '50%',
    height          : '10vh',
    backgroundColor : '#000',
    borderBottom    : '6px solid #fff',
    borderLeft      : '6px solid #fff',
    borderRight     : '6px solid #fff',
    margin          : '0px',
    'Container'     : {
      backgroundColor : '#fff',
      height          : '5vh',
      width           : '20vh',
      margin          : '0px',
    },
  },
  'SpeedBoxItem' : {
    fontWeight      : 'bold',
    color           : '#000',
    backgroundColor : '#fff',
    width           : '5vh',
    fontSize        : '2em',
    margin          : '0px',
  },
  'NoMargin' : {
    margin : '0px',
  },
  'InfoItem' : {
    fontWeight   : 'bold',
    color        : '#fff',
    fontSize     : '2.3em',
    borderBottom : '3px solid #fff',
    borderLeft   : '6px solid #fff',
    borderRight  : '6px solid #fff',
  },
  'redResult' : {
    color : 'red',
  },
  'blackResult' : {
    color           : '#000',
    backgroundColor : '#fff',
  },
  'greenResult' : {
    color : 'green',
  },
};

function checkResultStyle (result) {
  let returnStyle;
  if (result === 'black') {
    returnStyle = styles.blackResult;
  } else if (result === 'red') {
    returnStyle = styles.redResult;
  } else {
    returnStyle = styles.greenResult;
  }
  return returnStyle;
}

function rpmWheeltStyle (result) {
  let returnStyle;
  if (result >= '110' && result <= '200') {
    returnStyle = styles.greenResult;
  } else if (result < '110' || result > '200') {
    returnStyle = styles.redResult;
  } else {
    returnStyle = styles.greenResult;
  }
  return returnStyle;
}

function statusCircleStyle (result, circle) {
  let returnStyle;
  if ( (result === 'PLACE YOUR BETS' && circle === 'Go') || (result === 'ROTATE') ) {
    returnStyle = styles.HeaderRowCenter.go;
  } else if (result === 'LAST BET' && circle === 'Wait') {
    returnStyle = styles.HeaderRowCenter.wait;
  } else if (result === 'NO MORE BETS' && circle === 'Stop') {
    returnStyle = styles.HeaderRowCenter.stop;
  } else {
    returnStyle = styles.HeaderRowCenter.disable;
  }
  return returnStyle;
}

require('./style.scss');

@connectToStores
class Roulette extends Component {

  static propTypes = {
    'dealer'       : React.PropTypes.object,
    'game'         : React.PropTypes.object,
    'myPosition'   : React.PropTypes.number,
    'players'      : React.PropTypes.object,
    'winners'      : React.PropTypes.array,
    'winnersNames' : React.PropTypes.array,
  }

  componentWillMount () {
    RouletteActions.connect();
  }

  componentWillUnmount () {
    RouletteActions.disconnect();
  }

  static getStores () {
    return [ RouletteStore ];
  }

  static getPropsFromStores () {
    return RouletteStore.getState();
  }

  render () {
    let buttonsC;
    let buttons;
    let chat;
    let countPlayers;
    let winnersType;
    let winnersList;
    let winResult;

    const isDealer = window.location.hash.indexOf('dealer') !== -1;

    if (this.props.players[1] !== void 0) {
      countPlayers = Object.keys(this.props.players[1].connected).length - 1;
    } else {
      countPlayers = 0;
    }
    if (this.props.winners) {
      winnersType = this.props.winners.map(winner => {
        let objToReturn;
        const winn = winner.split('');
        if (winn[0] === '0' || winn[0] === '00') {
          if (winn.length === 2 ) {
            objToReturn = <span style={styles.Tie}>{winn[0]}</span>;
          } else {
            objToReturn = <span style={styles.Tie}>{winn[0] + winn[1]}</span>;
          }
        } else if (winn.length === 2 && winn[1] === 'B') {
          objToReturn = <span style={styles.Player}>{winn[0]}</span>;
        } else if (winn.length === 3 && winn[2] === 'B') {
          objToReturn = <span style={styles.Player}>{winn[0] + winn[1]}</span>;
        } else if (winn.length === 2 ) {
          objToReturn = <span style={styles.Banker}>{winn[0]}</span>;
        } else if (winn.length === 3) {
          objToReturn = <span style={styles.Banker}>{winn[0] + winn[1]}</span>;
        } else {
          objToReturn = <span style={styles.Banker}>{winn[0]}</span>;
        }
        return objToReturn;
      }
    );
    }
    if (this.props.winnersNames) {
      winnersList = this.props.winnersNames.map(name => <span>{name}</span>);
    }
    if (isDealer) {
      buttons = [];
      buttons.push(<Row>
        <Column text-center><DecisionButton action="MISTAKE" /></Column>
        <Column text-center><DecisionButton style={styles.DealerButtons} action="CHAT" /></Column>
        <Column text-center><DecisionButton style={styles.DealerButtons} action="ROULETTE" /></Column>
        <Column text-center><DecisionButton style={styles.DealerButtons} action="CHANGE ME" /></Column>
        </Row>);
      buttonsC = <CardScanner onScan={this._onScan} />;
    }
    if ((isDealer && this.props.dealer) || this.props.myPosition) {
      chat = (
          <Chat style={styles.ChatWindow}
            socket="http://10.28.10.85:1101/chat"
            room="RLTest"
            mode={isDealer ? 'dealer' : 'player'}
            user={isDealer ? this.props.dealer.nickname : AuthStore.state.user.NickName} />
      );
    } else {
      chat = <Column>Sit to see the chat</Column>;
    }

    if (this.props.game) {
      winResult = <span style={checkResultStyle(this.props.game.winner.color)}>{this.props.game.winner.number}</span>;
      if (this.props.game.action === 'PLACE YOUR BETS') {
        this._startProgress();
      }
      this.status = this.props.game.action;
      this.ballSpin = this.props.game.ballSpin;
      if ( (this.props.game.wheelRPM === '000') || (this.props.game.wheelRPM === '00')) {
        this.wheelRPM = '0';
      } else {
        this.wheelRPM = this.props.game.wheelRPM;
      }
    } else {
      this.status = '';
      this.ballSpin = '';
      this.wheelRPM = 0;
    }

    if (this.props.dealer !== null) {
      this.dealerName = this.props.dealer.nickname;
      this.spinsCounter = this.props.dealer.spinsCounter;
    } else {
      this.dealerName = 'No Dealer';
      this.spinsCounter = {'L' : 0, 'M' : 0, 'H' : 0, 'LdN' : 0, 'MdN' : 0, 'HdN' : 0};
    }

    return (
      <Grid>
        <Row >
          <Column />
          <Column />
          <Column text-right-padding>{buttons}</Column>
        </Row>
        <Row>
          <Column >
              <Row style={styles.SpeedBoxRow}>
                <Column style={styles.NoMargin}>
                    <Row style={styles.SpeedBoxRow.Container}>
                      <Column style={styles.SpeedBoxItem}>{this.spinsCounter.L}</Column>
                      <Column style={styles.SpeedBoxItem}>{this.spinsCounter.M}</Column>
                      <Column style={styles.SpeedBoxItem}>{this.spinsCounter.H}</Column>
                    </Row>
                </Column>
              </Row>
              <Row style={styles.SpeedBoxMiddle}>
                <Column style={styles.NoMargin}>
                    <Row style={styles.SpeedBoxMiddle.Container}>
                      <Column style={styles.SpeedBoxItem}>L</Column>
                      <Column style={styles.SpeedBoxItem}>M</Column>
                      <Column style={styles.SpeedBoxItem}>H</Column>
                    </Row>
                </Column>
              </Row>
              <Row style={styles.SpeedBoxRowEnd}>
                <Column style={styles.NoMargin}>
                    <Row style={styles.SpeedBoxRowEnd.Container}>
                      <Column style={styles.SpeedBoxItem}>{this.spinsCounter.LdN}</Column>
                      <Column style={styles.SpeedBoxItem}>{this.spinsCounter.MdN}</Column>
                      <Column style={styles.SpeedBoxItem}>{this.spinsCounter.HdN}</Column>
                    </Row>
                </Column>
              </Row>
              <Row style={styles.InfoItem}><Column text-center><span>{this.ballSpin}</span></Column></Row>
              <Row style={styles.InfoItem}>
                <Column text-center><span style={rpmWheeltStyle(this.wheelRPM)}>Speed: {this.wheelRPM} rpm</span></Column>
              </Row>
              <Row style={styles.TitleCol}><Column text-center>Winners List</Column></Row>
              <Row style={styles.SecondCol}>
                {winnersList}
              </Row>
              <Row style={styles.TitleCol}><Column text-center>Total Players</Column></Row>
              <Row style={styles.ThirdCol}><Column>{countPlayers}</Column></Row>
          </Column>
          <Column style={styles.CenterColumn}>
          <Grid>
              <Row style={styles.HeaderRowCenter}>
                <Column style={styles.HeaderRowCenter.info}>STATUS: {this.status} {winResult}</Column>
                <Column>
                  <div id="circleGo" style={statusCircleStyle(this.status, 'Go')}></div>
                </Column>
                <Column>
                  <div id="circleWait" style={statusCircleStyle(this.status, 'Wait')}></div>
                </Column>
                <Column>
                  <div id="circleStop" style={statusCircleStyle(this.status, 'Stop')}></div>
                </Column>
              </Row>
              <Row style={styles.HeaderSndRowCenter}><div id="container"></div></Row>
              <Row>
                {chat}
              </Row>
            </Grid>
            </Column>
            <Column>
              <Grid>
                <Row style={styles.InfoRow}>
                  <Column style={styles.InfoRow.Container}>
                    <div text-center style={styles.GameID}>Game ID: 12345</div>
                    <div text-center style={styles.GameID}>Dealer: {this.dealerName}</div>
                    <div text-center style={styles.GameID}>16:38</div>
                  </Column>
                </Row>
                <Row style={styles.TitleCol}><Column text-center>Set Number</Column></Row>
                <Row style={styles.RightColumn}>
                  {winnersType}
                </Row>
              </Grid>
            </Column>
            {buttonsC}

        </Row>
      </Grid>
    );
  }

  _startProgress = () => {
    const element = document.getElementById('container');
    const bar = new ProgressBar.Line(element, {
      strokeWidth : 3,
      trailWidth  : 1,
      color       : '#fff',
      trailColor  : '#000',
      duration    : 20000,
      text        : {
        style : {
          paddingLeft : '100vh',
        },
      },
    });

    bar.animate(1.0, {
      from : { color : '#00ff00' },
      to   : { color : '#ff0000' },
      step : (state) => {
        bar.path.setAttribute('stroke', state.color);
        bar.setText((bar.value() * 20).toFixed(0));
      },
    }, () => {
      bar.destroy();
    });
  }

  _onScan = card => {
    RouletteActions.addCard(card);
  }

}

export default Roulette;
