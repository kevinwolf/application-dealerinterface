import React, { Component } from 'react';
import { Hand } from 'apogeelive.ui.cards';

import RouletteActions from 'actions/RouletteActions';
import { ColumnRL } from 'components/ui/Layout';

const styles = {
  'PlayerSlot' : {
    backgroundColor : '#1d1d1b',
    display         : 'flex',
    flexDirection   : 'column',
    flexGrow        : 2,
  },
  'PlayerAction' : {
    backgroundColor : '#fff',
    padding         : 5,
  },
  'PlayerPosition' : {
    display         : 'inline-block',
    backgroundColor : 'blue',
    fontWeight      : 'bold',
    fontSize        : '2em',
    color           : '#fff',
  },
  'BankerPosition' : {
    display         : 'inline-block',
    backgroundColor : 'red',
    fontWeight      : 'bold',
    fontSize        : '2em',
    color           : '#fff',
  },
  'ResulStatus' : {
    backgroundColor : '#fff',
    padding         : 5,
    fontWeight      : 'bold',
    fontSize        : '2.3em',
  },
  'ResulStatusBanker' : {
    color : 'red',
  },
  'ResulStatusPlayer' : {
    color : 'blue',
  },
  'ResulStatusTie' : {
    color : 'green',
  },
  'PlayerCards' : {
    display  : 'flex',
    flexGrow : 1,
    padding  : 10,
  },
  'PlayerHand' : {
    float    : 'right',
    margin   : '0 30px',
    maxWidth : 100,
    width    : '100%',
  },
  'PlayerHand3' : {
    float    : 'left',
    margin   : '0 auto',
    maxWidth : 100,
    width    : '100%',
    // transform : 'rotate(90deg)',
  },
  'FirstDiv' : {
    display         : 'inline-block',
    backgroundColor : '#575755',
    color           : '#fff',
    fontWeight      : 'bold',
    fontSize        : '3em',
  },
  'PlayerScore' : {
    display         : 'inline-block',
    border          : '2.5px solid white',
    borderRadius    : '50%',
    padding         : '5px 15px',
    backgroundColor : '#000',
    color           : '#fff',
    fontWeight      : 'bold',
    fontSize        : '1.5em',
  },
};

function selectStyle (winner) {
  let styleToReturn;
  if (winner === 'TIE') {
    styleToReturn = styles.ResulStatusTie;
  } else if (winner === 'PLAYER') {
    styleToReturn = styles.ResulStatusPlayer;
  } else if (winner === 'BANKER') {
    styleToReturn = styles.ResulStatusBanker;
  }
  return styleToReturn;
}

function winnerResult (dealer, player) {
  let result;
  if (dealer === player) {
    result = `${dealer}/${player}`;
  } else if (dealer > player) {
    result = `${dealer}/${player}`;
  } else if (player > dealer) {
    result = `${player}/${dealer}`;
  } else {
    result `0/0`;
  }
  return result;
}

class DealerRTSlot extends Component {

  static propTypes = {
    'dataDealer'  : React.PropTypes.object,
    'dataPlayers' : React.PropTypes.object,
    'isDealer'    : React.PropTypes.bool,
    'position'    : React.PropTypes.number,
  }

  constructor (props) {
    super(props);
  }

  render () {
    let handPlayer;
    let handDealer;

    if (this.props.dataPlayers) {
      let cardsCount = 0;
      handPlayer = this.props.dataPlayers.cards.map(hand => {
        let objToReturn;
        cardsCount++;
        if (cardsCount < 3) {
          objToReturn = <div style={styles.PlayerHand}><Hand cards={hand} /></div>;
        } else {
          objToReturn = <div style={styles.PlayerHand3}><Hand cards={hand} /></div>;
        }
        return objToReturn;
      }
    );
    }

    if (this.props.dataDealer) {
      let cardsCount = 0;
      handDealer = this.props.dataDealer.cards.map(hand => {
        let objToReturn;
        cardsCount++;
        if (cardsCount < 3) {
          objToReturn = <div style={styles.PlayerHand}><Hand cards={hand} /></div>;
        } else {
          objToReturn = <div style={styles.PlayerHand3}><Hand cards={hand} /></div>;
        }
        return objToReturn;
      });
    }


    let slotContent;
    if (this.props.dataPlayers) {
      slotContent = [
        <div style={styles.FirstDiv}>{this.props.dataDealer.action}</div>,
        <div style={styles.ResulStatus}>RESULT <span style={selectStyle(this.props.dataDealer.winner)}>{this.props.dataDealer.winner} {winnerResult(this.props.dataDealer.hand.result, this.props.dataPlayers.hand.result)}</span></div>,
        <div style={styles.PlayerPosition}>PLAYER</div>,
        <div style={styles.PlayerCards}>{handPlayer}</div>,
        <div style={styles.BankerPosition}>BANKER</div>,
        <div style={styles.PlayerCards}>{handDealer}</div>,
      ];
    } else {
      slotContent = [
        <div style={styles.FirstDiv}>EXTRA CARD: BANKER</div>,
        <div style={styles.PlayerPosition}>PLAYER</div>,
        <div style={styles.PlayerCards}>{handPlayer}</div>,
        <div style={styles.BankerPosition}>BANKER</div>,
        <div style={styles.PlayerCards}>{handDealer}</div>,
      ];
    }

    return (
      <ColumnRL text-center style={styles.PlayerSlot}>{slotContent}</ColumnRL>
    );
  }

  _addPlayer = () => {
    RouletteActions.addPlayer(this.props.position);
  }

  _removePlayer = () => {
    RouletteActions.removePlayer(this.props.position);
  }

}

export default DealerRTSlot;
