import React, { Component } from 'react';
import BlackjackActions from 'actions/BlackjackActions';

require('./style.scss');
class DecisionButton extends Component {

  static propTypes = {
    'action' : React.PropTypes.string,
  }

  render () {
    return <button className="Button" onClick={this._sendDecision}>{this.props.action}</button>;
  }

  _sendDecision = () => {
    BlackjackActions.sendDecision(this.props.action);
  }

}

export default DecisionButton;
