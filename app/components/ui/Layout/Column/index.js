import React, { Component } from 'react';
import classNames from 'classnames';
require('./style.scss');

export default class Column extends Component {

  static propTypes = {
    'children'           : React.PropTypes.node.isRequired,
    'text-center'        : React.PropTypes.bool,
    'text-right'         : React.PropTypes.bool,
    'text-right-padding' : React.PropTypes.bool,
    'style'              : React.PropTypes.object,
  }

  constructor (props) {
    super(props);
  }

  render () {
    const classes = classNames({
      'Grid__Column'                     : true,
      'Grid__Column--text-center'        : this.props['text-center'],
      'Grid__Column--text-right'         : this.props['text-right'],
      'Grid__Column--text-right-padding' : this.props['text-right-padding'],
    });

    return <div className={classes} style={this.props.style}>{this.props.children}</div>;
  }

}
