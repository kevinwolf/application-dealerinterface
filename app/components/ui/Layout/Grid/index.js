import React, { Component } from 'react';
require('./style.scss');

export default class Grid extends Component {

  static propTypes = {
    'children' : React.PropTypes.node.isRequired,
  }

  render () {
    return <div className="Grid">{this.props.children}</div>;
  }

}
