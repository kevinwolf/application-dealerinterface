import React, { Component } from 'react';
import classNames from 'classnames';
require('./style.scss');

export default class Row extends Component {

  static propTypes = {
    'children'        : React.PropTypes.node.isRequired,
    'vertical-center' : React.PropTypes.bool,
    'style'           : React.PropTypes.object,
  }

  constructor (props) {
    super(props);
  }

  render () {
    const classes = classNames({
      'Grid__Row'                  : true,
      'Grid__Row--vertical-center' : this.props['vertical-center'],
    });

    return <div className={classes} style={this.props.style}>{this.props.children}</div>;
  }

}
