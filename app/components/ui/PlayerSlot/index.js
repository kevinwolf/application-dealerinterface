import React, { Component } from 'react';
import { Hand } from 'apogeelive.ui.cards';

import BlackjackActions from 'actions/BlackjackActions';
import BlackjackStore from 'stores/BlackjackStore';
import { Column } from 'components/ui/Layout';

function image (url) {
  return '#000 url(' + url + ') 100% 100%/100% 100% no-repeat ';
}
const styles = {
  'PlayerSlot' : {
    backgroundColor : '#1d1d1b',
    display         : 'flex',
    flexDirection   : 'column',
  },
  'PlayerName' : {
    background : image(require('./images/DataPlayerName.png')),
    color      : '#fff',
    flexShrink : 0,
    padding    : 5,
    fontWeight : 'bold',
    fontSize   : '1.3em',
  },
  'PlayerAction' : {
    backgroundColor : '#fff',
    padding         : 5,
  },
  'PlayerPosition' : {
    backgroundColor : '#fff',
    fontWeight      : 'bold',
    fontSize        : '1.5em',
  },
  'PlayerPositionSelected' : {
    backgroundColor : 'green',
    color           : '#fff',
    fontWeight      : 'bold',
    fontSize        : '1.5em',
  },
  'PlayerStatus' : {
    backgroundColor : '#ff0',
    padding         : 5,
  },
  'PlayerCards' : {
    display  : 'flex',
    flexGrow : 1,
    padding  : 10,
  },
  'PlayerHand' : {
    float    : 'right',
    margin   : '0 auto',
    maxWidth : 100,
    width    : '100%',
  },
  'PlayerScoreRow' : {
    flexShrink : 0,
    padding    : 5,
  },
  'PlayerScore' : {
    display         : 'inline-block',
    border          : '2.5px solid white',
    borderRadius    : '50%',
    padding         : '5px 15px',
    backgroundColor : '#000',
    color           : '#fff',
    fontWeight      : 'bold',
    fontSize        : '1.5em',
  },
};

class PlayerSlot extends Component {

  static propTypes = {
    'data'     : React.PropTypes.object,
    'isDealer' : React.PropTypes.bool,
    'position' : React.PropTypes.number,
  }

  constructor (props) {
    super(props);
  }

  render () {
    let hands;

    if (this.props.data) {
      hands = this.props.data.cards.map(hand => <div style={styles.PlayerHand}><Hand cards={hand} /></div>);
    }

    let slotContent;

    if (!this.props.data) {
      slotContent = <div style={styles.PlayerPosition}> {this.props.position}</div>;
    } else {
      slotContent = [
        <div style={this.props.data.currentTurn ? styles.PlayerPositionSelected : styles.PlayerPosition}> {this.props.position}</div>,
        <div style={styles.PlayerScoreRow}><span style={styles.PlayerScore}>{this.props.data && this.props.data.hand.result}</span></div>,
        <div style={styles.PlayerCards}>{hands}</div>,
        <div style={styles.PlayerAction}>{this.props.data.action}</div>,
        <div style={styles.PlayerStatus}>{this.props.data.hand.status}</div>,
        <div style={styles.PlayerName}>{this.props.data.nickname} - {this.props.data.balance}</div>,
      ];

      if (this.props.data.position === BlackjackStore.state.myPosition) {
        slotContent.push(<button onClick={this._removePlayer}>Exit</button>);
      }
    }

    return (
      <Column text-center style={styles.PlayerSlot}>{slotContent}</Column>
    );
  }

  _addPlayer = () => {
    BlackjackActions.addPlayer(this.props.position);
  }

  _removePlayer = () => {
    BlackjackActions.removePlayer(this.props.position);
  }

}

export default PlayerSlot;
