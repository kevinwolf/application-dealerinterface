// Third parties.
import React from 'react';
import { Route } from 'react-router';

// Layouts.
import Root from 'components/layouts/Root';
import App from 'components/layouts/App';

// Pages.
import Login from 'components/pages/Login';
import Blackjack from 'components/pages/Blackjack';
import Baccarat from 'components/pages/Baccarat';
import Roulette from 'components/pages/Roulette';

// Routes definition.
const Routes = (
  <Route handler={Root}>
    <Route name="app" path="/" handler={App}>
      <Route name="blackjack" handler={Blackjack} />
      <Route name="baccarat" handler={Baccarat} />
      <Route name="roulette" handler={Roulette} />
    </Route>

    <Route name="login" handler={Login} />
  </Route>
);

export default Routes;
