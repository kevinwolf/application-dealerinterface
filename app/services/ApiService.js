import axios from 'axios';
import AuthActions from 'actions/AuthActions';

const API_ENDPOINT = `${API_URL}/api`;
const ApiService = { };

ApiService.authenticate = {
  success : AuthActions.loginSuccessful,
  error   : AuthActions.loginFailed,
  remote  : (state, token) => axios.post(`${API_ENDPOINT}/customers/auth`, { token : token }),
};

export default ApiService;
