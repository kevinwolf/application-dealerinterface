let _router = null;
let _nextRoute = null;

export default {
  set          : router => _router = router,
  setNextRoute : route => _nextRoute = route,
  get          : () => _router,
  getNextRoute : () =>_nextRoute,
};
