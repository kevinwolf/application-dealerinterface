import { createStore, bind, datasource } from 'alt/utils/decorators';
import flux from 'flux';
import ApiService from 'services/ApiService';
import AuthActions from 'actions/AuthActions';

@createStore(flux)
@datasource(ApiService)
class AuthStore {

  constructor () {
    this.state = { token : null };
  }

  @bind(AuthActions.authenticate)
  authenticate (token) {
    this.getInstance().authenticate(token);
  }

  @bind(AuthActions.setUser)
  login (user) {
    this.setState({ user : user });
  }

  @bind(AuthActions.logout)
  logout () {
    this.setState({ user : null });
  }

}

export default AuthStore;
