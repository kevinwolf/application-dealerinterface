import { createStore, bind } from 'alt/utils/decorators';
import flux from 'flux';
import BaccaratActions from 'actions/BaccaratActions';

@createStore(flux)
class BaccaratStore {

  constructor () {
    this.state = {
      dealer     : null,
      players    : { },
      myPosition : null,
    };
  }

  @bind(BaccaratActions.addPlayer)
  setPlayer (position) {
    this.setState({ myPosition : position });
  }

  @bind(BaccaratActions.removePlayer)
  unSetPlayer () {
    this.setState({ myPosition : null });
  }

  @bind(BaccaratActions.getGameData)
  setGameData (gameData) {
    this.setState(gameData);
  }
}

export default BaccaratStore;
