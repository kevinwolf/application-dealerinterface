import { createStore, bind } from 'alt/utils/decorators';
import flux from 'flux';
import BlackjackActions from 'actions/BlackjackActions';

@createStore(flux)
class BlackjackStore {

  constructor () {
    this.state = {
      dealer     : null,
      players    : { },
      myPosition : null,
    };
  }

  @bind(BlackjackActions.addPlayer)
  setPlayer (position) {
    this.setState({ myPosition : position });
  }

  @bind(BlackjackActions.removePlayer)
  unSetPlayer () {
    this.setState({ myPosition : null });
  }

  @bind(BlackjackActions.getGameData)
  setGameData (gameData) {
    this.setState(gameData);
  }
}

export default BlackjackStore;
