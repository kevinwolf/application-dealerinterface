import { createStore, bind } from 'alt/utils/decorators';
import flux from 'flux';
import RouletteActions from 'actions/RouletteActions';

@createStore(flux)
class RouletteStore {

  constructor () {
    this.state = {
      dealer     : null,
      players    : { },
      myPosition : null,
    };
  }

  @bind(RouletteActions.addPlayer)
  setPlayer (position) {
    this.setState({ myPosition : position });
  }

  @bind(RouletteActions.removePlayer)
  unSetPlayer () {
    this.setState({ myPosition : null });
  }

  @bind(RouletteActions.getGameData)
  setGameData (gameData) {
    this.setState(gameData);
  }
}

export default RouletteStore;
