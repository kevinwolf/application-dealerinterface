#include<pthread.h>
#include<stdio.h>
#include<termios.h>
#include<unistd.h>
#include<errno.h>
#include<fcntl.h>
#include<sys/time.h>
#include <stdbool.h>
#include<string.h>
#include<stdlib.h>
#include<libwebsockets.h>
#include<stdbool.h>

#define BAUDRATE B9600
#define PORT "/dev/ttyS1"
#define TRUE 1
#define FALSE 0
#define POCKET_NO	"NXXX"
//#define WHEEL_RPM	"RXXX"
#define BALL_SPINS	"BSXX"
#define STOP_BET	"STOP"
#define START_BET	"STRT"
#define LAST_BET        "LBET"
#define ROTATE          "ROTE"
#define HISTORY		"HISTORY"
#define	STATE		"STATE"
#define	WRPM		"WRPM"

//ERRORS
#define INVALID_WIN     "E001"

int i;
int n = 0;
int sl = 0;
int connections = 0;
int state = -1;
int lenw = 4;
bool sentRpm = false;
struct lws_context_creation_info info;
struct libwebsocket_context *context;
char results[29] = "99-99-99-99-99-99-99-99-99-99";
char ballspuns[3] = "";
char LWS_MESSAGE[] = "STRT";
char WHEEL_RPM[] = "RXXX";
typedef enum{start_bet,rotate,last_bet,stop_bet,win_bet} game_state1;
game_state1 game_state = start_bet;
pthread_mutex_t lock;

static int callback_http(struct libwebsocket_context * this,
		struct libwebsocket *wsi,
		enum libwebsocket_callback_reasons reason, void *user,
		void *in, size_t len)
{
	return 0;
}



static int callback_dumb_increment(struct libwebsocket_context * this,
		struct libwebsocket *wsi,
		enum libwebsocket_callback_reasons reason,
		void *user, void *in, size_t len)
{
	switch (reason)
	{
		case LWS_CALLBACK_ESTABLISHED:
			{
				len = 9;
				printf("\nconnection established\n");
				unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING + len +
						LWS_SEND_BUFFER_POST_PADDING);
				connections++;
				char Msg[] = "CONNECTED";
				int i;
				for (i=0; i < len; i++)
				{
					buf[LWS_SEND_BUFFER_PRE_PADDING + i ] = Msg[i];
				}
				libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], len, LWS_WRITE_TEXT);
				free(buf);
				break;
			}
		case LWS_CALLBACK_RECEIVE:
			{
				int i;
				printf("\n Message Recived from Client  is %s",(char*)in);
				if(strcmp((char*)in,STATE) == 0)
				{
					if(state == 0)
					{
						int len1 = 4;
						unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING+
								len1 +	LWS_SEND_BUFFER_POST_PADDING);
						char msg[] = "OPEN";
						for (i = 0; i < len1; i++)
							buf[LWS_SEND_BUFFER_PRE_PADDING +  i ] = msg[i];
						libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], len1,
								LWS_WRITE_TEXT);
						free(buf);
					}
					else if (state == 1)
					{
						int len2 = 5;
						unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING+
								len2 +	LWS_SEND_BUFFER_POST_PADDING);
						char msg[] = "CLOSE";
						for (i = 0; i < len2; i++)
							buf[LWS_SEND_BUFFER_PRE_PADDING +  i ] = msg[i];
						libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], len2,
								LWS_WRITE_TEXT);
						free(buf);
					}
				}
				else if(strcmp((char*)in,HISTORY) == 0)
				{
					printf("\n *************Sending last 10 results*****************");
					int len3 = 29;
					unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING + len3 +
							LWS_SEND_BUFFER_POST_PADDING);
					for (i = 0; i < len3; i++)
						buf[LWS_SEND_BUFFER_PRE_PADDING + i ] = results[i];
					libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], len3, LWS_WRITE_TEXT);
					free(buf);
				}
				else if(strcmp((char*)in,WRPM) == 0)
				{
					printf("\n *************Sending Roulette speed*****************");
					int len3 = 4;
					unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING + len3 +
							LWS_SEND_BUFFER_POST_PADDING);
					for (i = 0; i < len3; i++)
						buf[LWS_SEND_BUFFER_PRE_PADDING + i ] = WHEEL_RPM[i];
					libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], len3, LWS_WRITE_TEXT);
					free(buf);
				}
				else
				{
					printf("\n Invalid Message sent from Client %s ",(char*)in);
					int len4 = 15;
					unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING + len4 +
							LWS_SEND_BUFFER_POST_PADDING);
					char msg[] = "INVALID_MESSAGE";
					for (i = 0; i < len4; i++)
						buf[LWS_SEND_BUFFER_PRE_PADDING +  i ] = msg[i];
					libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], len4, LWS_WRITE_TEXT);
					free(buf);
				}
				break;
			}
		case LWS_CALLBACK_SERVER_WRITEABLE:
			{
				printf("\nMessage sending  to Client %s ",LWS_MESSAGE);
				unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING + lenw +
						LWS_SEND_BUFFER_POST_PADDING);
				int i;
				for (i=0; i < lenw; i++)
				{
					buf[LWS_SEND_BUFFER_PRE_PADDING + i ] = LWS_MESSAGE[i];
				}
				libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], lenw, LWS_WRITE_TEXT);
				free(buf);
				break;
			}
		case LWS_CALLBACK_CLOSED:
			{
				printf("\n********A Connections Closed *********");
				connections--;
			}
		default:
			break;
	}
	return 0;
}

static struct libwebsocket_protocols protocols[] = {
	/* first protocol must always be HTTP handler */
	{
		"http-only",   // name
		callback_http, // callback
		0              // per_session_data_size
	},
	{
		"dumb-increment-protocol", // protocol name - very important!
		callback_dumb_increment,   // callback
		0                          // we don't use any per session data
	},
	{
		NULL, NULL, 0   /* End of list */
	}
};


void *ClientMonitoring()
{
	//websocket configuration..
	int port = 9000;
	const char *interface = NULL;
	// we're not using ssl
	const char *cert_path = NULL;
	const char *key_path = NULL;
	int opts = 0;
	//struct lws_context_creation_info info;
	memset(&info,0,sizeof(info));
	info.port = port;
	info.iface = interface;
	info.protocols = protocols;
	info.extensions = libwebsocket_get_internal_extensions();
	info.ssl_cert_filepath = NULL;
	info.ssl_private_key_filepath = NULL;
	info.gid = -1;
	info.uid = -1;
	info.options = opts;
	context = libwebsocket_create_context(&info);
	if (context == NULL) {
		fprintf(stderr, "libwebsocket init failed\n");
		return -1;
	}
	state = -1;
	printf("\n\nstarting server\n\n");
	while(1)
	{
		libwebsocket_service(context,0);
		//this will monitor clients connections and receive messages from clients
	}
}


void *GameFlow()
{
	printf("\nStarting Game flow");
	while(1)
	{
		if( connections >0)
		{
			if(game_state == win_bet)
			{
				sleep(5); //%seconds delay between game cycles
				game_state = start_bet;
			}
			if(game_state == start_bet)
			{
				state = 0;
				strcpy(LWS_MESSAGE,START_BET);
				libwebsocket_callback_on_writable_all_protocol(protocols + 1);
				sleep(15);
				pthread_mutex_lock(&lock);
				game_state = rotate;
				pthread_mutex_unlock(&lock);
			}
			if(game_state == rotate)
			{
				strcpy(LWS_MESSAGE,ROTATE);
				libwebsocket_callback_on_writable_all_protocol(protocols + 1);
				sleep(7);
				pthread_mutex_lock(&lock);
				game_state = last_bet;
				pthread_mutex_unlock(&lock);
			}
			if(game_state == last_bet)
			{
				strcpy(LWS_MESSAGE,LAST_BET);
				libwebsocket_callback_on_writable_all_protocol(protocols + 1);
				sleep(3);
				state = 1;
				pthread_mutex_lock(&lock);
				game_state = stop_bet;
				pthread_mutex_unlock(&lock);
				strcpy(LWS_MESSAGE,STOP_BET);
				libwebsocket_callback_on_writable_all_protocol(protocols + 1);
			}
		}

	}
}


//int getGameState(char c)
//{
//	if(c == '2' || c == '3')
//		return 0; //Start_BET
//	if(c == '4')
//		return 1; //stop_BET
//	return -1;
//}
//void setGameState()
//{
//	if(state == 0)
//		strcpy(LWS_MESSAGE, START);
//	else if(state == 1)
//		strcpy(LWS_MESSAGE, STOP);
//	if( state == 0 || state ==1 )
//		libwebsocket_callback_on_writable_all_protocol(protocols + 1);
//	return;
//}


bool processMessage( char mon[42])
{
	if( mon[3] == '4')
	{
		char tempStr[3] = "";
		tempStr[0] = mon[25];
		tempStr[1] = mon[26];
		char testStr[3] = "00";
		if(strcmp(tempStr,testStr) != 0)
		{
			ballspuns[0] = tempStr[0];
			ballspuns[1] = tempStr[1];
		}
	}
	WHEEL_RPM[1] = mon[14];
	WHEEL_RPM[2] = mon[15];
	WHEEL_RPM[3] = mon[16];

	strcpy(LWS_MESSAGE,WHEEL_RPM);
	LWS_MESSAGE[1] = mon[14];
	LWS_MESSAGE[2] = mon[15];
	LWS_MESSAGE[3] = mon[16];
	libwebsocket_callback_on_writable_all_protocol(protocols + 1);
	if(game_state == win_bet && sentRpm)
	{
		//Seding Ball spuns in case of a valid win
		sleep(2);
		strcpy(LWS_MESSAGE,BALL_SPINS);
		LWS_MESSAGE[2] = ballspuns[0];#include<pthread.h>
#include<stdio.h>
#include<termios.h>
#include<unistd.h>
#include<errno.h>
#include<fcntl.h>
#include<sys/time.h>
#include <stdbool.h>
#include<string.h>
#include<stdlib.h>
#include<libwebsockets.h>
#include<stdbool.h>

#define BAUDRATE B9600
#define PORT "/dev/ttyS1"
#define TRUE 1
#define FALSE 0
#define POCKET_NO	"NXXX"
//#define WHEEL_RPM	"RXXX"
#define BALL_SPINS	"BSXX"
#define STOP_BET	"STOP"
#define START_BET	"STRT"
#define LAST_BET        "LBET"
#define ROTATE          "ROTE"
#define HISTORY		"HISTORY"
#define	STATE		"STATE"
#define	WRPM		"WRPM"

//ERRORS
#define INVALID_WIN     "E001"

int i;
int n = 0;
int sl = 0;
int connections = 0;
int state = -1;
int lenw = 4;
bool sentRpm = false;
struct lws_context_creation_info info;
struct libwebsocket_context *context;
char results[29] = "99-99-99-99-99-99-99-99-99-99";
char ballspuns[3] = "";
char LWS_MESSAGE[] = "STRT";
char WHEEL_RPM[] = "RXXX";
typedef enum{start_bet,rotate,last_bet,stop_bet,win_bet} game_state1;
game_state1 game_state = start_bet;
pthread_mutex_t lock;

static int callback_http(struct libwebsocket_context * this,
		struct libwebsocket *wsi,
		enum libwebsocket_callback_reasons reason, void *user,
		void *in, size_t len)
{
	return 0;
}



static int callback_dumb_increment(struct libwebsocket_context * this,
		struct libwebsocket *wsi,
		enum libwebsocket_callback_reasons reason,
		void *user, void *in, size_t len)
{
	switch (reason)
	{
		case LWS_CALLBACK_ESTABLISHED:
			{
				len = 9;
				printf("\nconnection established\n");
				unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING + len +
						LWS_SEND_BUFFER_POST_PADDING);
				connections++;
				char Msg[] = "CONNECTED";
				int i;
				for (i=0; i < len; i++)
				{
					buf[LWS_SEND_BUFFER_PRE_PADDING + i ] = Msg[i];
				}
				libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], len, LWS_WRITE_TEXT);
				free(buf);
				break;
			}
		case LWS_CALLBACK_RECEIVE:
			{
				int i;
				printf("\n Message Recived from Client  is %s",(char*)in);
				if(strcmp((char*)in,STATE) == 0)
				{
					if(state == 0)
					{
						int len1 = 4;
						unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING+
								len1 +	LWS_SEND_BUFFER_POST_PADDING);
						char msg[] = "OPEN";
						for (i = 0; i < len1; i++)
							buf[LWS_SEND_BUFFER_PRE_PADDING +  i ] = msg[i];
						libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], len1,
								LWS_WRITE_TEXT);
						free(buf);
					}
					else if (state == 1)
					{
						int len2 = 5;
						unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING+
								len2 +	LWS_SEND_BUFFER_POST_PADDING);
						char msg[] = "CLOSE";
						for (i = 0; i < len2; i++)
							buf[LWS_SEND_BUFFER_PRE_PADDING +  i ] = msg[i];
						libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], len2,
								LWS_WRITE_TEXT);
						free(buf);
					}
				}
				else if(strcmp((char*)in,HISTORY) == 0)
				{
					printf("\n *************Sending last 10 results*****************");
					int len3 = 29;
					unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING + len3 +
							LWS_SEND_BUFFER_POST_PADDING);
					for (i = 0; i < len3; i++)
						buf[LWS_SEND_BUFFER_PRE_PADDING + i ] = results[i];
					libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], len3, LWS_WRITE_TEXT);
					free(buf);
				}
				else if(strcmp((char*)in,WRPM) == 0)
				{
					printf("\n *************Sending Roulette speed*****************");
					int len3 = 4;
					unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING + len3 +
							LWS_SEND_BUFFER_POST_PADDING);
					for (i = 0; i < len3; i++)
						buf[LWS_SEND_BUFFER_PRE_PADDING + i ] = WHEEL_RPM[i];
					libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], len3, LWS_WRITE_TEXT);
					free(buf);
				}
				else
				{
					printf("\n Invalid Message sent from Client %s ",(char*)in);
					int len4 = 15;
					unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING + len4 +
							LWS_SEND_BUFFER_POST_PADDING);
					char msg[] = "INVALID_MESSAGE";
					for (i = 0; i < len4; i++)
						buf[LWS_SEND_BUFFER_PRE_PADDING +  i ] = msg[i];
					libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], len4, LWS_WRITE_TEXT);
					free(buf);
				}
				break;
			}
		case LWS_CALLBACK_SERVER_WRITEABLE:
			{
				printf("\nMessage sending  to Client %s ",LWS_MESSAGE);
				unsigned char *buf = (unsigned char*) malloc(LWS_SEND_BUFFER_PRE_PADDING + lenw +
						LWS_SEND_BUFFER_POST_PADDING);
				int i;
				for (i=0; i < lenw; i++)
				{
					buf[LWS_SEND_BUFFER_PRE_PADDING + i ] = LWS_MESSAGE[i];
				}
				libwebsocket_write(wsi, &buf[LWS_SEND_BUFFER_PRE_PADDING], lenw, LWS_WRITE_TEXT);
				free(buf);
				break;
			}
		case LWS_CALLBACK_CLOSED:
			{
				printf("\n********A Connections Closed *********");
				connections--;
			}
		default:
			break;
	}
	return 0;
}

static struct libwebsocket_protocols protocols[] = {
	/* first protocol must always be HTTP handler */
	{
		"http-only",   // name
		callback_http, // callback
		0              // per_session_data_size
	},
	{
		"dumb-increment-protocol", // protocol name - very important!
		callback_dumb_increment,   // callback
		0                          // we don't use any per session data
	},
	{
		NULL, NULL, 0   /* End of list */
	}
};


void *ClientMonitoring()
{
	//websocket configuration..
	int port = 9000;
	const char *interface = NULL;
	// we're not using ssl
	const char *cert_path = NULL;
	const char *key_path = NULL;
	int opts = 0;
	//struct lws_context_creation_info info;
	memset(&info,0,sizeof(info));
	info.port = port;
	info.iface = interface;
	info.protocols = protocols;
	info.extensions = libwebsocket_get_internal_extensions();
	info.ssl_cert_filepath = NULL;
	info.ssl_private_key_filepath = NULL;
	info.gid = -1;
	info.uid = -1;
	info.options = opts;
	context = libwebsocket_create_context(&info);
	if (context == NULL) {
		fprintf(stderr, "libwebsocket init failed\n");
		return -1;
	}
	state = -1;
	printf("\n\nstarting server\n\n");
	while(1)
	{
		libwebsocket_service(context,0);
		//this will monitor clients connections and receive messages from clients
	}
}


void *GameFlow()
{
	printf("\nStarting Game flow");
	while(1)
	{
		if( connections >0)
		{
			if(game_state == win_bet)
			{
				sleep(5); //%seconds delay between game cycles
				game_state = start_bet;
			}
			if(game_state == start_bet)
			{
				state = 0;
				strcpy(LWS_MESSAGE,START_BET);
				libwebsocket_callback_on_writable_all_protocol(protocols + 1);
				sleep(15);
				pthread_mutex_lock(&lock);
				game_state = rotate;
				pthread_mutex_unlock(&lock);
			}
			if(game_state == rotate)
			{
				strcpy(LWS_MESSAGE,ROTATE);
				libwebsocket_callback_on_writable_all_protocol(protocols + 1);
				sleep(7);
				pthread_mutex_lock(&lock);
				game_state = last_bet;
				pthread_mutex_unlock(&lock);
			}
			if(game_state == last_bet)
			{
				strcpy(LWS_MESSAGE,LAST_BET);
				libwebsocket_callback_on_writable_all_protocol(protocols + 1);
				sleep(3);
				state = 1;
				pthread_mutex_lock(&lock);
				game_state = stop_bet;
				pthread_mutex_unlock(&lock);
				strcpy(LWS_MESSAGE,STOP_BET);
				libwebsocket_callback_on_writable_all_protocol(protocols + 1);
			}
		}

	}
}


//int getGameState(char c)
//{
//	if(c == '2' || c == '3')
//		return 0; //Start_BET
//	if(c == '4')
//		return 1; //stop_BET
//	return -1;
//}
//void setGameState()
//{
//	if(state == 0)
//		strcpy(LWS_MESSAGE, START);
//	else if(state == 1)
//		strcpy(LWS_MESSAGE, STOP);
//	if( state == 0 || state ==1 )
//		libwebsocket_callback_on_writable_all_protocol(protocols + 1);
//	return;
//}


bool processMessage( char mon[42])
{
	if( mon[3] == '4')
	{
		char tempStr[3] = "";
		tempStr[0] = mon[25];
		tempStr[1] = mon[26];
		char testStr[3] = "00";
		if(strcmp(tempStr,testStr) != 0)
		{
			ballspuns[0] = tempStr[0];
			ballspuns[1] = tempStr[1];
		}
	}
	WHEEL_RPM[1] = mon[14];
	WHEEL_RPM[2] = mon[15];
	WHEEL_RPM[3] = mon[16];

	strcpy(LWS_MESSAGE,WHEEL_RPM);
	LWS_MESSAGE[1] = mon[14];
	LWS_MESSAGE[2] = mon[15];
	LWS_MESSAGE[3] = mon[16];
	libwebsocket_callback_on_writable_all_protocol(protocols + 1);
	if(game_state == win_bet && sentRpm)
	{
		//Seding Ball spuns in case of a valid win
		sleep(2);
		strcpy(LWS_MESSAGE,BALL_SPINS);
		LWS_MESSAGE[2] = ballspuns[0];
		LWS_MESSAGE[3] = ballspuns[1];
		libwebsocket_callback_on_writable_all_protocol(protocols + 1);
		sentRpm = false;
	}
	if(mon[12] == '0')
	{
		//int i = getGameState(mon[3]);
		//		if(state != i && (i == 0 || i == 1))
		//		{
		//			state = i;
		//			setGameState();
		//		}
		return TRUE; //no errors
	}
	else
	{
		if(mon[12] == '2')
			{
			//	printf("\nValue of warning flag -%c\n",mon[12]);
			strcpy(LWS_MESSAGE,INVALID_WIN);
			LWS_MESSAGE[2] = 'S';
			LWS_MESSAGE[3] = 'D';
			libwebsocket_callback_on_writable_all_protocol(protocols + 1);
			printf("Ball is traveling in the same direction as the rotor");
			return FALSE;
			}
			else
			{
			printf("\nValue of warninng flag -%c\n",mon[12]);
			return FALSE;
			}
	}
}



int main()
{
	//Thread to send messages to Client
	pthread_t t1,t2;
	pthread_create(&t1, NULL,&ClientMonitoring ,NULL);
	pthread_create(&t2, NULL,&GameFlow ,NULL);
	printf("\n\ntest program to read the port \n\n");
	struct termios port_settings;
	int fd;
	unsigned char msgS[5]="",msgR[10];
	fd = open(PORT,O_RDWR|O_NOCTTY|O_SYNC);
	if(fd == -1)
	{
		printf("Failed to open PORT: %s  \n\n",PORT);
		perror("Error:");
	}
	bzero(&port_settings, sizeof(port_settings));
	cfsetispeed(&port_settings, BAUDRATE);
	cfsetospeed(&port_settings, BAUDRATE);
	port_settings.c_cflag = (port_settings.c_cflag & ~CSIZE) | CS8;
	port_settings.c_iflag &= ~IGNBRK;
	port_settings.c_lflag = 0;
	port_settings.c_oflag = 0;
	port_settings.c_cc[VMIN]  = 0;
	port_settings.c_cc[VTIME] = 5;
	port_settings.c_iflag &= ~(IXON | IXOFF | IXANY);
	port_settings.c_cflag |= (CLOCAL | CREAD);
	port_settings.c_cflag &= ~(PARENB | PARODD);
	port_settings.c_cflag |= 0;
	port_settings.c_cflag &= ~CSTOPB;
	port_settings.c_cflag &= ~CRTSCTS;
	port_settings.c_lflag |= (ICANON);
	if (tcsetattr (fd, TCSANOW, &port_settings) != 0)
	{
		//error_message ("error %d from tcsetattr", errno);
		return -1;
	}
	char mon[42] = "";
	while(1)
	{
		int n = read (fd, &mon,sizeof(mon));
		if(n == 0)
		{
			perror("Error while READ:");
			return 0;
		}
		else if(n == 4)
		{
			sentRpm = true;
			printf("\nWINNING NUMBER-%s",mon);
			strcpy(LWS_MESSAGE, POCKET_NO);
			LWS_MESSAGE[2] = mon[0];
			LWS_MESSAGE[3] = mon[1];
			if(game_state != stop_bet)
			{
				strcpy(LWS_MESSAGE,INVALID_WIN);
				game_state = start_bet;
			}
			else
			{
				pthread_mutex_lock(&lock);
				game_state = win_bet;
				pthread_mutex_unlock(&lock);
			}
			libwebsocket_callback_on_writable_all_protocol(protocols + 1);
			//inserting the result in the history array
			//shifting entries in history
			i = 24;
			while(i>=0)
			{
				results[i+3] = results[i];
				results[i+4] = results[i+1];
				i = i-3;
			}
			results[0] = mon[0];
			results[1] = mon[1];
			continue;
		}
		printf("MSG RCVD: %s",mon);
		if (!processMessage(mon))
		{
			//RETRY
			continue;
		}
	}
	close(fd);
	libwebsocket_context_destroy(context);
	return 0;
}

		LWS_MESSAGE[3] = ballspuns[1];
		libwebsocket_callback_on_writable_all_protocol(protocols + 1);
		sentRpm = false;
	}
	if(mon[12] == '0')
	{
		//int i = getGameState(mon[3]);
		//		if(state != i && (i == 0 || i == 1))
		//		{
		//			state = i;
		//			setGameState();
		//		}
		return TRUE; //no errors
	}
	else
	{
		if(mon[12] == '2')
			{
			//	printf("\nValue of warning flag -%c\n",mon[12]);
			strcpy(LWS_MESSAGE,INVALID_WIN);
			LWS_MESSAGE[2] = 'S';
			LWS_MESSAGE[3] = 'D';
			libwebsocket_callback_on_writable_all_protocol(protocols + 1);
			printf("Ball is traveling in the same direction as the rotor");
			return FALSE;
			}
			else
			{
			printf("\nValue of warninng flag -%c\n",mon[12]);
			return FALSE;
			}
	}
}



int main()
{
	//Thread to send messages to Client
	pthread_t t1,t2;
	pthread_create(&t1, NULL,&ClientMonitoring ,NULL);
	pthread_create(&t2, NULL,&GameFlow ,NULL);
	printf("\n\ntest program to read the port \n\n");
	struct termios port_settings;
	int fd;
	unsigned char msgS[5]="",msgR[10];
	fd = open(PORT,O_RDWR|O_NOCTTY|O_SYNC);
	if(fd == -1)
	{
		printf("Failed to open PORT: %s  \n\n",PORT);
		perror("Error:");
	}
	bzero(&port_settings, sizeof(port_settings));
	cfsetispeed(&port_settings, BAUDRATE);
	cfsetospeed(&port_settings, BAUDRATE);
	port_settings.c_cflag = (port_settings.c_cflag & ~CSIZE) | CS8;
	port_settings.c_iflag &= ~IGNBRK;
	port_settings.c_lflag = 0;
	port_settings.c_oflag = 0;
	port_settings.c_cc[VMIN]  = 0;
	port_settings.c_cc[VTIME] = 5;
	port_settings.c_iflag &= ~(IXON | IXOFF | IXANY);
	port_settings.c_cflag |= (CLOCAL | CREAD);
	port_settings.c_cflag &= ~(PARENB | PARODD);
	port_settings.c_cflag |= 0;
	port_settings.c_cflag &= ~CSTOPB;
	port_settings.c_cflag &= ~CRTSCTS;
	port_settings.c_lflag |= (ICANON);
	if (tcsetattr (fd, TCSANOW, &port_settings) != 0)
	{
		//error_message ("error %d from tcsetattr", errno);
		return -1;
	}
	char mon[42] = "";
	while(1)
	{
		int n = read (fd, &mon,sizeof(mon));
		if(n == 0)
		{
			perror("Error while READ:");
			return 0;
		}
		else if(n == 4)
		{
			sentRpm = true;
			printf("\nWINNING NUMBER-%s",mon);
			strcpy(LWS_MESSAGE, POCKET_NO);
			LWS_MESSAGE[2] = mon[0];
			LWS_MESSAGE[3] = mon[1];
			if(game_state != stop_bet)
			{
				strcpy(LWS_MESSAGE,INVALID_WIN);
				game_state = start_bet;
			}
			else
			{
				pthread_mutex_lock(&lock);
				game_state = win_bet;
				pthread_mutex_unlock(&lock);
			}
			libwebsocket_callback_on_writable_all_protocol(protocols + 1);
			//inserting the result in the history array
			//shifting entries in history
			i = 24;
			while(i>=0)
			{
				results[i+3] = results[i];
				results[i+4] = results[i+1];
				i = i-3;
			}
			results[0] = mon[0];
			results[1] = mon[1];
			continue;
		}
		printf("MSG RCVD: %s",mon);
		if (!processMessage(mon))
		{
			//RETRY
			continue;
		}
	}
	close(fd);
	libwebsocket_context_destroy(context);
	return 0;
}
