var connect     = require('connect')
  , serveStatic = require('serve-static')
  , port        = require('../package.json').port;

console.log(port);
console.log(__dirname);
connect().use(serveStatic(__dirname)).listen(port);
